# Requirements Document Template

Authors: Raffaele Stelluti, Francesco Pavan, Leonardo Severi, Davide Taddei

Date: 08_04_2019

Version: 0.1

# Contents

- [Assumptions](#assumptions)
- [Stakeholders](#stakeholders)
- [Context Diagram and interfaces](#context-diagram-and-interfaces)
	+ [Context Diagram](#context-diagram)
	+ [Interfaces](#interfaces)

- [Stories and personas](#stories-and-personas)
- [Functional and non functional requirements](#functional-and-non-functional-requirements)
	+ [Functional Requirements](#functional-requirements)
	+ [Non functional requirements](#non-functional-requirements)
- [Use case diagram and use cases](#use-case-diagram-and-use-cases)
	+ [Use case diagram](#use-case-diagram)
	+ [Use cases](#use-cases)
	+ [Relevant scenarios](#relevant-scenarios)
- [Glossary](#glossary)
- [System design](#system-design)

# Assumptions

- There is not a capsule vending machine
- The system is made up of two GUIs: one for the manager and one for the employees (on their computers). A visitor does not have an interface, to buy capsules he has to ask directly to the manager. There is not a public interface for visitors to order capsules
- The manager buys boxes of capsules and sells them to employees and visitors. The purchase is completed with a payment using the manager's banking system
- Employees buy capsules without interacting with their interface, is the manager who is in charge to sell the capsules and to update the system
- Each vendor has public API in its buying platform, which is exploited to order boxes of capsules
- Data are stored in a central server and each user interface connects to it to retrieve informations
- The system is oriented to a limited environment (15-20 employees)
- The system does not perform any check on consistency and coherence of an account (the manager is responsible of it)

# Stakeholders

| Stakeholder name  | Description |
| ----------------- |:-----------:|
| Employee          | Works in the company and wants to drink coffee |
| Visitor           | Doesn't work in the company but sometimes is inside the company building and wants some coffee |
| Manager           | He is in charge of managing the system. He buys boxes of capsules and sells single capsules |
| Capsules Vendor   | Company which sells capsules |

# Context Diagram and interfaces

## Context Diagram

```plantuml

left to right direction
skinparam packageStyle rectangle

actor Vendor as v
actor Manager as m
actor Employee as e
actor Bank as b
rectangle system {
  (#LaTazza) as l
   v -- l
   l -- m
   l -- e
   l -- b
}

```

## Interfaces
| Actor | Logical Interface | Physical Interface  |
| ------------- |:-------------:| -----:|
| Employee | GUI | PC |
| Manager | GUI | PC |
| Vendor | API | Internet |
| Bank | API | Internet |

# Stories and personas

## Persona 1 (Employee)

Tony Smith

30, junior developer of the company.

As a developer, Tony has to work in front of a computer most of his working hours. His main occupation concerns the calendar application of the company, which he has to maintain and improve.

Currently he has to purchase coffee capsules personally, wasting a lot of time to find the vendor and paying an extra fee due to shipping costs. Shipping costs could be avoided by buying more capsules in one single order, but it is difficult to manage a bigger purchase with collegues.

Goals:
- wants to order capsules more efficiently, without wasting a lot of time
- wants to avoid shipping cost, buying larger amount of capsules in one order

## Persona 2 (Manager)

Richard Cosby

55, he has been working for the company for twenty years and he always try to group togheter as many order as possible.

In the last twenty years he purchased box of capsules in two different ways: going to the vendors personally or buying capsules calling every vendor one by one. Moreover he had to keep track of all purchases of single capsules made by every employee.

Goals:
- Wants to manage the orders more efficiently
- Wants an automatic system to keep track of every purchase made by an employee or a by a visitor

## Persona 3 (Visitor)

Tin Cheng

43, Quality control employee

Tin will spend one week in a company branch in order to check work quality and employee productivity.
Tin has a strong addiction towards caffeine but he have not capsules for shared coffee maker and only way
to take a coffee is the very far bar.

Goals:
- wants a small amount of capsules to use shared coffee maker
- wants save some money

# Functional and non functional requirements

## Functional Requirements

| ID        | Description  |
| --------- |:-------------:|
|	FR1		| The manager shall be able to order boxes and manage orders |
|	FR2		| The manager shall be able to check all Employee’s balances |
|	FR3		| The manager shall be able to check the inventory |
|	FR4		| The manager shall be able to check the cash available|
|	FR5		| The manager shall be able to sell capsules|
|	FR6		| An employee shall be able to check his balance |
|	FR7		| The system shall keep track about capsules directly sold to visitors |
|	FR8		| The employee application should check the availability of capsules |
|	FR9		| The manager side app should be able to notify if capsules of a certain type are less than a given threshold |
|	FR10	| The employee side app should be able to notify if his debts are more than a given threshold |
|	FR11	| The manager shall be able to view all the purchases |
|	FR12	| The manager shall be able to remove employee from the system |
|	FR13	| The manager side app should be able to show and manage the cash |
|	FR14	| The manager shall be able to update capsules' type and quantity in the inventory |
|	FR15	| The manager should be able to send a solicitation to empolyees who have debts |
|	FR16	| The system shall deny any operation if a quantity in the inventory becomes less than 0 |
|	FR17	| The manager should have available statistics about consumptions |
|	FR18	| The employee side app shall allow a new employee to sign up |
|	FR19	| When an account is removed, no money is given back or requested if balance is less than zero	|
|	FR20	| System shall be able to retrive list of available vendor	|

## Non Functional Requirements

| ID        | Type (efficiency, reliability, ..)  | Description  | Refers to |
| --------- |:-----------------------------------:| :-----------:| --------: |
| 	NFR1    |  Usability	| The time so learn how to use the system for an unexperienced user (someone who've never worked in IT area) should be less than ten minutes | / |
|	NFR2	|  Portability	| The system shall run under PCs' operating systems (Windows, Linux, macOS) | / |
|	NFR3	|  Domain		| The currency shall be the euro | FR2, FR4, FR6, FR10, FR13 | / |
|	NFR4	|  Reliability	| The system should fail less than once a year | / |
|	NFR5	|  Speed		| Response time (for all interfaces) should be less than 0.5 second | / |
|	NFR6	|  Interoperability |  The system should be able to interact with coffee vendors | FR1 |
|	NFR7	|  Speed		| The interface of the employee side app should react in less than 1 second to changes in the inventory (also about types of items) | FR8 |

# Use case diagram and use cases

## Use case diagram

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Manager as m
actor Employee as e
actor Visitor as v
actor Bank as b
actor Vendor as ven


(Buy capsules with money) <. (Buy capsules):include
e --> (Buy capsules)
v --> (Buy capsules with money)


m -->(Updating of an employee's pocket)

(Managing of employee's account) .> (Remove an employee's account):include
(Managing of employee's account) .> (Adding an employee's account):include
e --> (Adding an employee's account)
m --> (Remove an employee's account)
m --> (Managing of employee's account)

m --> (Order boxes)

(Order boxes) --> b
(Order boxes) --> ven
```

## Use Cases

### Updating of an employee's pocket, UC1

| Actors Involved        | Employee, Manager |
| ------------- |:-------------:|
|  Precondition     | User is registered on laTazza System |  
|  Post condition     |The manager updates the balance in the employee's account |
|  Nominal Scenario     | This use case decribe the update of the employee's pocket. It happens when an employee buys credits from the manager or when he pays off his debt |
|  Variants     | If employee have not enough money, system do not any update |

### Managing of an employee's account, UC2

| Actors Involved        | Employee, Manager |
| ------------- |:-------------:|
|  Precondition     | A user starts working for the company or he leaves it |
|  Post condition     |User account is added or removed |
|  Nominal Scenario     | Employee create his account, only manager can remove account from the system|
|  Variants     | If an error occurs the action will be repeated after some time |

### Buy capsules, UC3

| Actors Involved        |  Manager |
| ------------- |:-------------:|
|  Precondition     | Manager must be in the office |
|  Post condition     | Visitor or employee receive capsules and credits and invetory are consistent|
|  Nominal Scenario     | After an employee/visitor requests the manager provide capsules |
|  Variants     | If there are no capsules or Visitor have not enough money transaction interrupts |

### Order boxes, UC4

| Actors Involved        | Employee, Manager |
| ------------- |:-------------:|
|	Precondition		|	The manager has enough money to purchase new boxes OR there is cash in the inventory |
|	Post condition		| There are new capsules in the inventory |
|	Nominal Scenario	|	This use case describes the process of ordering boxes by the manager. The system has an interface with a vendor and when the delivery is done, the inventory is automatically updated. |
|	Variants			| If an error occurs, also due to a vendor mistake, a rollback must be performed by the system and nothing changes in the inventory. |

# Relevant scenarios

## Purchase of credits

Precondition: user must work for the company.

Post condition: manager loads the corresponding credits in the employee's account.

| Scenario ID: SC1        | Corresponds to UC: updating of an employee's pocket (UC1) |
| ------------- |:-------------:|
| Step#  | Description  |
|  1     | The manager takes the cash of the employee |
|  2     | The manager loads the purchased credits in the employee's account |
|  3     | The system updates the balance of the employee |
|  4     | The system updates the total available cash |

## Sending solicitation for a debt

Precondition: user must work for the company and he has a negative balance.

Postcondition: user receives a notification which confirms the correct payment.

| Scenario ID: SC2        | Corresponds to UC: updating of an employee's pocket (UC1) |
| ------------- |:-------------:|
| Step#        | Description  |
|	1	|	The manager looks at his interface and notices an employee with a negative balance	|
|	2	|	The manager sends a payment solicitation to the employee through his interface |
|	3	|	The employee receives the notification on his interface |

## Adding an employee's account

Precondition: user starts to work for the company or an employee wants to use the coffee maker again.

Post condition: user adds its new account in the capsule management system.

| Scenario ID: SC3        | Corresponds to UC: managing of employee's account (UC2) |
| ------------- |:-------------:|
| Step#        | Description  |
|  1     | An employee opens LaTazza interface for the first time, or after its account has been removed by the manager |  
|  2     | System asks user to insert his name, surname, username and password |
|  3     | System checks that user doesn't exist yet, and adds it to the capsules management system |

Variants:

| Step#        | Description  |
| ------------- |:-------------:|
| 3.A | If the username already exists in the system, new user is not added and the operation fails |

## Removing an employee's account

Precondition: user leaves the company or an employee does not want to use the coffee maker anymore.

Post condition: employee's account is removed from the system.

| Scenario ID: SC4        | Corresponds to UC: managing of employee's account (UC2) |
| ------------- |:-------------:|
| Step#        | Description  |
|  1     | Manager receives a request from an employee to remove his personal account |
|  2     | Manager removes the employee's account using LaTazza interface |
|  3     | System removes the data of the employee and his balance |

Variants:

| Step#        | Description  |
| ------------- |:-------------:|
| 3.A | If employee's account has already been deleted the operation fails |

## Buying capules with credit

Precondition: Manager must be in office and employee request for capsules

Post condition: Employee get capsules, number of available capsules and employee's credit are updated

| Scenario ID: SC5        | Corresponds to UC: buy capsules (UC3)|
| ------------- |:-------------:|
| Step#        | Description  |
|	1	|	The employee goes to the manager office and asks him for capsules |
|	2	| 	Manager selects employee account on the system and insert desired number of capsule and unit price|
|	3	| 	System calculates new employee credit and updates it and number of available capsules|

Variants:

| Step#        | Description  |
| ------------- |:-------------:|
| 2.A | Number of desired capsules is greater than available capsules, the manager interrupts |


## Employee buys capules with cash

Precondition: Manager must be in office,employee requests for capsules and have enough money

Post condition: Employee get capsules, number of available capsules is updated

| Scenario ID: SC6        | Corresponds to UC: buy capsules (UC3) |
| ------------- |:-------------:|
| Step#        | Description  |
|	1	|	The employee goes to the manager office and asks him for capsules |
|	2	|	Manager chooses the right employee |
|	3	|	Manager chooses the type and quantity of capsules and cash as pay method |
|	4	|	System updates number of available capsules and total cash |


Variants:

| Step#        | Description  |
| ------------- |:-------------:|
|	3.A | Number of desired capsules is greater than available capsules, the manager interrupts |

## Visitor buys capsules with cash

Precondition: Manager must be in office,visitor requests for capsules and have enough money

Post condition: Visitor get capsules, number of available capsules is updated

| Scenario ID: SC7        | Corresponds to UC: buy capsules (UC3) |
| ------------- |:-------------:|
| Step#        | Description  |
|	1	|	The visitor goes to the manager office and asks him for capsules |
|	2	| Manager inserts in the system desired number of capsule and unit price |
|	3	| System updates number of available capsules and total cash |


Variants:

| Step#        | Description  |
| ------------- |:-------------:|
| 2.A | Number of desired capsules is greater than available capsules, the manager interrupts |

## Order boxes

Precondition: the company is running out of capsules and there are enough money to purchase new boxes of capsules

Postcondition: the system inventary is updated with the new quantity of capsules

| Scenario ID: SC8        | Corresponds to UC: order boxes (UC4) |
| ------------- |:-------------:|
| Step#        | Description  |
|	1	|	The system retrieves the list of the available vendors |
|	2	|	The manager chooses an available vendor |
|	3	|	The manager chooses a type of boxes and the quantity and submits the order |
|	4	|	The system sends the order to the vendor |
|	5	|	The vendor notifies each change of status to the system |
|	6	|	The vendor notifies when the delivery is done and the system updates the inventory |
|	7	|	The system logs the order	|

# Glossary

```plantuml
class Employee{
	+username
	+name
	+surname
}
class Visitor {
}
class Manager {
}
class Pocket {
	+credit
}
class Capsule {
	+type
}
class Order{
	+ID
	+Vendor
	+Quantity
	+Total cost
	+Status
	+Timestamp
}
class Box {
}
Employee "1" -- "*" Capsule:buy
Employee "1" -- "1" Pocket:has
Capsule o--"50" Box:contains
Manager "1" -- "*" Order:perform
Order "1" -- "*" Box:contains
Visitor "1" -- "1..*" Capsule:buy
```

# System Design

```plantuml

class CapsulesManagement {
}

class CentralServer {
	+showCapsulePreferences()
	+writeLog()
}

class ManagerGUI {
	+buyBoxes()
	+checkInventory()
	+checkCash()
	+showEmployeeBalance()
	+sellCapsules()
	+loadCredits()
	+removeEmployeeAccount()
	+employeeSolicitation()
}

class EmployeeGUI {
	+showCapsulesAvailability()
	+showBalance()
	+createPersonalAccount()
}

class BankingGateway {
	+processPayment()
}

ManagerGUI -- BankingGateway
CentralServer -- ManagerGUI
CentralServer -- EmployeeGUI
CapsulesManagement o-- CentralServer

```
